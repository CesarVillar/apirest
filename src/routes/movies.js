const {Router} = require("express");

const router = Router();
const  _ = require("underscore");

const movies = require("../sample.json");
//console.log(movies);

//router.get('/movies', ( req, res)  => {
//    res.send('movies');
//});
router.get('/', ( req, res)  => {
    //res.send('movies');
    res.json(movies);
});

router.post('/', ( req, res)  => {
    //console.log(req.body);
    const {title,year,rating} = req.body;
    if( title && year && rating) {
        const id = movies.length + 1 ;
        const newMovie = {...req.body,id};
       // console.log(newMovie);
        movies.push(newMovie);
        res.json(movies);
       // res.json('saved');
    }
    else {
       // res.json('wrong ');
       //res.json({error:"hubo un error"});
       res.status(500).json({error:"hubo un error"});
     }
     res.send('recibido');
});

router.put('/:id', ( req, res)  => {
    const { id } = req.params;
    const {title,year,rating} = req.body;
    if( title && year && rating) {
     _.each(movies, (movie, i) => {
       if (movie.id == id) {
        movie.title = title;
        movie.year = year;
        movie.rating = rating;
       }
    });
    res.json(movies);
    } else{
       res.status(500).json({error:"hubo un error"});
    }
  
 });
router.delete('/:id', ( req, res)  => {
     const { id } = req.params;
     //console.log(req.params);
    //res.send("eliminado");
    _.each(movies, (movie, i) => {
        if (movie.id == id) {
         movies.splice(i,1);
        }
    });
    res.send("eliminado");
    //res.send(movies);
  });

module.exports  = router;

