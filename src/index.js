const express = require ("express");

const app = express();

const morgan = require("morgan");

/*setting*/
app.set('port', process.env.PORT ||3000);
app.set('json spaces', 2);


/*middleware */
/*-app.use(morgan('combined')); */
app.use(morgan('dev')); 
app.use(express.urlencoded({extended: false}) ); /*recibir datos input de formularios*/
app.use(express.json()); /*recibir formatos json*/

//routes
//app.get('/', (req,res) => {
//   //res.send("Hola mundo")
 //  res.json({"Titulo:" : "Hola mundo"})
//});

//app.use('/api/movies',require("./routes/index"));
app.use(require("./routes/index"));

app.use('/api/movies',require("./routes/movies"));
app.use('/api/users',require("./routes/users"));

/* iniciar servidor*/
app.listen( app.get('port'), () => {
 console.log(`server on port ${app.get('port')}`);

} );
